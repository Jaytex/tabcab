package ca.tabcab.tabcab;

import java.io.Serializable;

/**
 * Created by jay on 28/11/15.
 */
public class TabCabLocation implements Serializable {
    private static final long serialVersionUID = 0L;
    public final double lat;
    public final double lng;
    public final String emailAddress;

    TabCabLocation(double lat, double lng, String emailAddress) {
        this.lat = lat;
        this.lng = lng;
        this.emailAddress = emailAddress;
    }

    public double getLatitude() {
        return lat;
    }

    public double getLongitude() {
        return lng;
    }

    public String getEmailAddress(){
        return emailAddress;
    }
}
