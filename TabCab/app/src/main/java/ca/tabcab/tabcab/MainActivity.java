package ca.tabcab.tabcab;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Strategy;

import java.util.HashMap;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "MainActivity";
    private Context mContext = this;
    private Message mMessage;
    private MessageListener mMessageListener;
    private boolean mResolvingError = false;
    private boolean isSubscribingEnabled = false;
    private boolean isPublishingEnabled = false;
    private Handler handler;
    private LocationManager mLocationManager;
    private static int numLocationChanges = 0;
    public static boolean hasRequested = false;
    // The minimum distance to change Updates in meters
    private static final long LOCATION_REFRESH_DISTANCE = 0; // 0 meters

    // The minimum time between updates in milliseconds
    private static final long LOCATION_REFRESH_TIME = 0; // 1 minute

    private HashMap<String, Marker> others; // email to location map

    /**
     * Sets the time in seconds for a published message or a subscription to live. Set to max (1 day).
     */
    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(Strategy.TTL_SECONDS_MAX).build();

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    protected LatLng myLocation;
    private GoogleMap map;
    SupportMapFragment smf;
    FloatingActionButton fab;
    Marker me;
    TabCabLocation currentOtherLocation = null;
    String emailAddress = "blank";
    Marker destinationMarker;

    private final android.location.LocationListener mLocationListener = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            if (mGoogleApiClient.isConnected()) {
                numLocationChanges++;
                Log.d(TAG, "I am moving");

                double lat = location.getLatitude();
                double lng = location.getLongitude();
                Log.d(TAG, "Received GPS request for " + String.valueOf(lat) + "," + String.valueOf(lng) + " , ready to rumble!");

                myLocation = new LatLng(lat, lng);
                CameraUpdate center = CameraUpdateFactory.newLatLng(myLocation);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(20);
                me.setPosition(myLocation);
                map.moveCamera(center);

                TabCabLocation tabCabLocation = new TabCabLocation(lat, lng, emailAddress);
                byte[] serializedLoc = NearbyLocationHelper.serializeLocation(tabCabLocation);
                mMessage = new Message(serializedLoc);
                publish(mMessage);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        buildGoogleApiClient();
        others = new HashMap<>();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(mContext).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                emailAddress = account.name;
            }
        }
        Log.i(TAG, "My EmailAddress: " + emailAddress);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, mLocationListener);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_icon));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (others.isEmpty()){
                    CharSequence text = "There are no drivers in range!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast.makeText(mContext, text, duration).show();
                    publish(mMessage);
                    subscribe();
                    text = "Searching for drivers...";
                    Toast.makeText(mContext, text, duration).show();
                } else {
                    CharSequence text = "Select Destination";
                    int duration = Toast.LENGTH_SHORT;
                    Toast.makeText(mContext, text, duration).show();
                    Intent intent = new Intent(mContext, LocationRequestActivity.class);
                    startActivity(intent);
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        handler = new Handler();

    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "stop()");
        if (mGoogleApiClient.isConnected()) {
            if (isPublishingEnabled) {
                Nearby.Messages.unpublish(mGoogleApiClient, mMessage)
                        .setResultCallback(new ErrorCheckingCallback("unpublish()"));
                isPublishingEnabled = false;
            }
            if (isSubscribingEnabled) {
                Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                        .setResultCallback(new ErrorCheckingCallback("unsubscribe()"));
                isSubscribingEnabled = false;
            }
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }


    public void initMap() {
        Log.d(TAG, "initMap");

        smf = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content_main, smf, "map").commit();

        fab.setVisibility(View.VISIBLE);

        handler.post(new Runnable() {
            @Override
            public void run() {
                map = smf.getMap();
                CameraUpdate center = CameraUpdateFactory.newLatLng(myLocation);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(20);
                me = map.addMarker(new MarkerOptions()
                        .position(myLocation)
                        .title("Current Location")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_pickup)));
                map.moveCamera(center);
                map.animateCamera(zoom);

                if (hasRequested) {
                    Log.d(TAG, "Setting destination marker");
                    LatLng destination = new LatLng(myLocation.latitude +.0008, myLocation.longitude + .0005);
                    destinationMarker  = map.addMarker(new MarkerOptions()
                            .position(destination)
                            .title("Destination")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_destination)));
                    map.animateCamera(zoom);

                    fab.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_x));
                    fab.setBackgroundTintList(ContextCompat.getColorStateList(mContext, R.color.red));
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CharSequence text = "Request canceled!";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(mContext, text, duration).show();
                            destinationMarker.remove();
                            fab.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_icon));
                            fab.setBackgroundTintList(ContextCompat.getColorStateList(mContext, R.color.teal));
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (others.isEmpty()) {
                                        CharSequence text = "There are no drivers in range!";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast.makeText(mContext, text, duration).show();
                                        publish(mMessage);
                                        subscribe();
                                        text = "Searching for drivers...";
                                        Toast.makeText(mContext, text, duration).show();
                                    } else {
                                        CharSequence text = "Select Destination";
                                        int duration = Toast.LENGTH_SHORT;
                                        Toast.makeText(mContext, text, duration).show();
                                        Intent intent = new Intent(mContext, LocationRequestActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }
                        });

                    hasRequested = false;
                }

                publish(mMessage);
                subscribe();
            }

            // use nearby handler to se up location stuff for the other device
        });
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Nearby.MESSAGES_API)
                .build();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (id) {
            case R.id.nav_main:
                initMap();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.nav_cc:
                fragment = new CreditFragment();
                break;
            case R.id.nav_transactions:
                fragment = new TransactionFragment();
                break;
            case R.id.nav_promo:
                fragment = new PromoFragment();
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;
            case R.id.nav_about:
                fragment = new AboutFragment();
                break;
        }

        fab.setVisibility(View.INVISIBLE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content_main, fragment).commit();
        getSupportFragmentManager().executePendingTransactions();
        transaction.addToBackStack(null);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Connected to GoogleApiClient");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            myLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            TabCabLocation location = new TabCabLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), emailAddress);
            byte[] locationSerialized = NearbyLocationHelper.serializeLocation(location);
            mMessage = new Message(locationSerialized);
            publish(mMessage);
            initMap(); // only initialize map once you have current position
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            Log.d(TAG, "Couldn't retrieve Location");
        }

        Nearby.Messages.getPermissionStatus(mGoogleApiClient).setResultCallback(
                new ErrorCheckingCallback("getPermissionStatus", new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "run()");
                        if (isPublishingEnabled) {
                            publish(mMessage);
                        }
                        if (isSubscribingEnabled) {
                            subscribe();
                        }
                    }
                })
        );
    }

    synchronized public void publish(Message message) {
        Log.i(TAG, "publish()");

        if (isPublishingEnabled) {
            Nearby.Messages.unpublish(mGoogleApiClient, mMessage)
                    .setResultCallback(new ErrorCheckingCallback("unpublish()"));
            isPublishingEnabled = false;
        }

        mMessage = message;
        isPublishingEnabled = true;

        if (!mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Attempted to publish() before GoogleApiClient is connected. Waiting for connection.");
            return;
        }

        // TODO: Use PublishOptions constructor instead of deprecated constructor
//        PublishOptions options = new PublishOptions.Builder()
//                .setStrategy(PUB_SUB_STRATEGY)
//                .build();
        Nearby.Messages.publish(mGoogleApiClient, message, PUB_SUB_STRATEGY)
                .setResultCallback(new ErrorCheckingCallback("publish()"));
    }

    synchronized public void subscribe() {
        Log.i(TAG, "subscribe()");

        if (isSubscribingEnabled) {
            Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                    .setResultCallback(new ErrorCheckingCallback("unsubscribe()"));
            isSubscribingEnabled = false;
        }

        isSubscribingEnabled = true;

        if (mMessageListener == null) {
            // Create an instance of MessageListener
            mMessageListener = new MessageListener() {
                @Override
                public void onFound(final Message message) {
                    Log.i(TAG, "Message received, size = " + message.getContent().length);
                    final TabCabLocation location = NearbyLocationHelper.deserializeLocation(message.getContent());
                    LatLng latLng = null;

                    if (location != null) {
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        Log.i(TAG, "Received Location: (" + (location.getLatitude()) + ", "
                                + (location.getLongitude()) + ")" + " with email address: " + location.getEmailAddress());

                        final LatLng finalLatLng = latLng;

                        ((Activity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (currentOtherLocation == null) {
                                    Log.d(TAG, "Setting current tab cab location");
                                    currentOtherLocation = location;
                                }

                                if (others.get(location.getEmailAddress()) != null) {
                                    Log.d(TAG, "Receiving message from a registered device!");
                                    others.get(location.getEmailAddress()).setPosition(finalLatLng);
                                } else {
                                    Log.d(TAG, " Received a location from a new device with email: " + location.getEmailAddress());
                                    Marker other =  map.addMarker(new MarkerOptions()
                                            .position(finalLatLng)
                                            .title(location.getEmailAddress())
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_car_near)));

                                    others.put(location.getEmailAddress(), other);
                                    for (String email : others.keySet()) {
                                        Log.d(TAG, "Registered Email: " + email);
                                    }
                                }
                            }
                        });
                    }
                }

//                @Override
//            public void onLost(final Message message) {
//                    Log.i(TAG, "Message lost, size = " + message.getContent().length);
//                    final TabCabLocation location = NearbyLocationHelper.deserializeLocation(message.getContent());
//                    if (location != null) {
//                        Log.i(TAG, "Lost location with email address: " + location.getEmailAddress());
//                    }
//                    ((Activity) mContext).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (location != null) {
//                                others.get(location.getEmailAddress()).remove();
//                                others.remove(location.getEmailAddress());
//                            }
//                        }
//                    });
//                }
            };
        }

        if (!mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Attempted to subscribe() before GoogleApiClient is connected. Waiting for connection.");
            return;
        }

        // TODO: Use PublishOptions constructor instead of deprecated constructor
//        PublishOptions options = new PublishOptions.Builder()
//                .setStrategy(PUB_SUB_STRATEGY)
//                .build();
        Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, PUB_SUB_STRATEGY)
                .setResultCallback(new ErrorCheckingCallback("subscribe()"));
    }



    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "I see you moving from old lc");
    }

    /**
     * A simple ResultCallback that displays a toast when errors occur.
     * It also displays the Nearby opt-in dialog when necessary.
     */
    private class ErrorCheckingCallback implements ResultCallback<Status> {
        private final String method;
        private final Runnable runOnSuccess;

        /**
         * Request code to use when launching the resolution activity.
         */
        static final int REQUEST_RESOLVE_ERROR = 1001;

        private ErrorCheckingCallback(String method) {
            this(method, null);
        }

        private ErrorCheckingCallback(String method, @Nullable Runnable runOnSuccess) {
            this.method = method;
            this.runOnSuccess = runOnSuccess;
        }

        @Override
        public void onResult(@NonNull Status status) {
            Log.i(TAG, "onResult: " + method);
            if (status.isSuccess()) {
                Log.i(TAG, method + " succeeded.");
                if (runOnSuccess != null) {
                    runOnSuccess.run();
                }
            } else {
                // Currently, the only resolvable error is that the device is not opted
                // in to Nearby. Starting the resolution displays an opt-in dialog.
                if (status.hasResolution()) {
                    if (!mResolvingError) {
                        try {
                            Log.i(TAG, "starting resolution");
                            status.startResolutionForResult((Activity)mContext,
                                    REQUEST_RESOLVE_ERROR);
                            mResolvingError = true;
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, method + " failed with exception: " + e);
                        }
                    } else {
                        // This will be encountered on initial startup because we do
                        // both publish and subscribe together.  So having a toast while
                        // resolving dialog is in progress is confusing, so just log it.
                        Log.i(TAG, method + " failed with status: " + status
                                + " while resolving error.");
                    }
                } else {
                    Log.e(TAG, method + " failed with : " + status
                            + " resolving error: " + mResolvingError);
                }
            }
        }
    }

}
