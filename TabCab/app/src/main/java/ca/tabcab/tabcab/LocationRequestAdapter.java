package ca.tabcab.tabcab;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jay on 05/12/15.
 */
public class LocationRequestAdapter extends BaseAdapter {

    private ArrayList<LocationRequestObject> locationRequestObjects;
    private final LayoutInflater mLayoutInflater;
    private Context mContext;

    LocationRequestAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setLocationRequestObjects(ArrayList<LocationRequestObject> locationRequestObjects) {
        this.locationRequestObjects = locationRequestObjects;
    }

    @Override
    public int getCount() {
        return locationRequestObjects.size();
    }

    @Override
    public LocationRequestObject getItem(int position) {
        return locationRequestObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class LocationRequestHolder {
        TextView name, address, distance;
        ImageView icon;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LocationRequestHolder locationRequestHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.location_request_item, parent, false);
            locationRequestHolder = new LocationRequestHolder();

            locationRequestHolder.name = (TextView) convertView.findViewById(R.id.name);
            locationRequestHolder.address = (TextView) convertView.findViewById(R.id.address);
            locationRequestHolder.distance = (TextView) convertView.findViewById(R.id.distance);
            locationRequestHolder.icon = (ImageView) convertView.findViewById(R.id.icon);

            convertView.setTag(locationRequestHolder);

        } else {
            locationRequestHolder = (LocationRequestHolder) convertView.getTag();
        }

        LocationRequestObject locationRequestObject = getItem(position);

        locationRequestHolder.name.setText(locationRequestObject.name);
        int color  = 0;
        color = locationRequestObject.isSpecial ? ContextCompat.getColor(mContext, R.color.teal) : ContextCompat.getColor(mContext, R.color.grey);

        locationRequestHolder.name.setTextColor(color);
        locationRequestHolder.address.setText(locationRequestObject.address);
        locationRequestHolder.distance.setText(locationRequestObject.distance);
        locationRequestHolder.icon.setImageDrawable(locationRequestObject.icon);

        return convertView;
    }
}
