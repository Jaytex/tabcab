package ca.tabcab.tabcab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by jay on 15/11/15.
 */
public class LoginActivity extends Activity {

    LinearLayout linearLayout;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_layout);
        inflater =  LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.login_layout, null);
        linearLayout = (LinearLayout) findViewById(R.id.login_details);
        linearLayout.addView(view);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void skipScreen(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void loginScreen(View view) {
        linearLayout.removeAllViews();
        View view1 = inflater.inflate(R.layout.inner_login_layout, null);
        linearLayout.addView(view1);
    }

    public void signUpScreen(View view){
        linearLayout.removeAllViews();
        View view1 = inflater.inflate(R.layout.sign_up_layout, null);
        linearLayout.addView(view1);
    }

    public void signUp(View view) {
        Context context = getApplicationContext();
        CharSequence success = "Account has been made!";
        CharSequence failure = "Please fill in all fields!";
        int duration = Toast.LENGTH_SHORT;

        if (true) {
            Toast.makeText(context, success, duration).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
             Toast.makeText(context, failure, duration).show();
        }
    }

    public void login(View view) {
        Context context = getApplicationContext();
        CharSequence success = "Login Successful!";
        CharSequence failure = "Incorrect username or password.";
        int duration = Toast.LENGTH_SHORT;

        if (true) {
            Toast.makeText(context, success, duration).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(context, failure, duration).show();
        }
    }
}
