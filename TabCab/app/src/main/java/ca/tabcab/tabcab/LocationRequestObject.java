package ca.tabcab.tabcab;

import android.graphics.drawable.Drawable;

/**
 * Created by jay on 05/12/15.
 */
public class LocationRequestObject {
    String name;
    String address;
    String distance;
    Drawable icon;
    boolean isSpecial;

    LocationRequestObject(String name, String address, float distance, Drawable icon, boolean isSpecial) {
        this.name = name;
        this.address = address;
        this.distance = distance > 1000 ? (distance/1000) + " km" : (int) distance + " m";
        this.icon = icon;
        this.isSpecial = isSpecial;
    }
}
