package ca.tabcab.tabcab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by jay on 05/12/15.
 */
public class LocationRequestFragment extends Fragment {
    private String title;
    private int page;
    private LocationRequestAdapter locationRequestAdapter;
    public Context mContext;

    public static LocationRequestFragment newInstance(String title, int page, LocationRequestAdapter locationRequestAdapter, Context context) {
        LocationRequestFragment locationRequestFragment = new LocationRequestFragment();
        locationRequestFragment.setLocationRequestAdapter(locationRequestAdapter);
        locationRequestFragment.mContext = context;
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        locationRequestFragment.setArguments(args);
        return locationRequestFragment;
    }

    public void setLocationRequestAdapter(LocationRequestAdapter locationRequestAdapter) {
        this.locationRequestAdapter = locationRequestAdapter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("page");
        title = getArguments().getString("title");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.location_request_fragment, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CharSequence text = "Destination confirmed! Please wait for your pick up.";
                int duration = Toast.LENGTH_SHORT;
                Toast.makeText(mContext, text, duration).show();
                MainActivity.hasRequested = true;
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
            }
        });
        listView.setAdapter(locationRequestAdapter);
        return view;
    }
}
