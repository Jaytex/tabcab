package ca.tabcab.tabcab;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created by jay on 23/11/15.
 */
public class NearbyLocationHelper {

    public static byte[] serializeLocation(TabCabLocation location) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        byte[] yourBytes = new byte[0];
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(location);
            yourBytes = bos.toByteArray();
            out.close();

            return yourBytes;
        } catch (IOException ex) {
            System.out.println("Location Serialization failed. " + ex);
        }

        return null;
    }

    public static TabCabLocation deserializeLocation(byte[] byteArray) {
        ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
        ObjectInput in = null;
        Object deserializedContact = null;
        try {
            in = new ObjectInputStream(bis);
            deserializedContact = in.readObject();
            in.close();
            if (deserializedContact instanceof TabCabLocation) {
                return ((TabCabLocation) deserializedContact);
            } else {
                throw new ClassCastException("Unknown object type: " + deserializedContact.getClass());
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Location Deserialization failed. " + ex);
        }

        return null;
    }
}
