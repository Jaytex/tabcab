package ca.tabcab.tabcab;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by jay on 05/12/15.
 */
public class LocationRequestPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    private Context mContext;

    private static final String RECENTS = "Recents";
    private static final String FAVOURITES = "Favourites";
    private static final String NEAR = "Near";

    public LocationRequestPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Fragment getItem(int position) {
        ArrayList<LocationRequestObject> locationRequestObjects = new ArrayList<>();
        LocationRequestAdapter locationRequestAdapter = new LocationRequestAdapter(mContext);
        Drawable icon  = mContext.getDrawable(R.drawable.pin_destination);
        String page = "";
        switch (position) {
            case 0:
                locationRequestObjects.add(new LocationRequestObject("Home", "11 Home Address Ave", 110, icon, true));
                locationRequestObjects.add(new LocationRequestObject("The Office", "8 Wall Street Lane", 345, icon, true));
                locationRequestObjects.add(new LocationRequestObject("Jane & Finch Intersection", "1518 Finch Street", 380, icon, false));
                locationRequestObjects.add(new LocationRequestObject("McDonalds", "King and Columbia", 699, icon, false));
                locationRequestObjects.add(new LocationRequestObject("Bomb Shelter Pub", "200 University Avenue", 1300, icon, false));
                locationRequestObjects.add(new LocationRequestObject("Starbucks Cafe", "110 Overpriced Trail", 1400, icon, false));
                locationRequestObjects.add(new LocationRequestObject("Kinkaku Izakaya", "217 King St W", 2100, icon, false));
                locationRequestAdapter.setLocationRequestObjects(locationRequestObjects);
                page = RECENTS;
                break;
            case 1:
                locationRequestObjects.add(new LocationRequestObject("Home", "11 Home Address Ave", 110, icon, false));
                locationRequestObjects.add(new LocationRequestObject("The Office", "8 Wall Street Lane", 345, icon, false));
                locationRequestAdapter.setLocationRequestObjects(locationRequestObjects);
                page = FAVOURITES;
                break;
            case 2:
                locationRequestObjects.add(new LocationRequestObject("Home", "11 Home Address Ave", 110, icon, true));
                locationRequestAdapter.setLocationRequestObjects(locationRequestObjects);
                page = NEAR;
                break;
            default:
                return null;

        }
        return LocationRequestFragment.newInstance(page, position, locationRequestAdapter, mContext);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return RECENTS;
            case 1:
                return FAVOURITES;
            case 2:
                return NEAR;
            default:
                return "Page";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
